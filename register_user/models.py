from __future__ import unicode_literals

from django.db import models

# Create your models here.
class User(models.Model):
    email = models.CharField(primary_key=True, max_length=50)
    password = models.CharField(max_length=50)
    nama = models.CharField(max_length=100)
    alamat_lengkap = models.TextField()

    class Meta:
        db_table = 'user'

class Sponsor(models.Model):
    email = models.ForeignKey('User', models.CASCADE, db_column='email', primary_key=True)
    logo_sponsor = models.ImageField()

    class Meta:
        db_table = 'sponsor'

class Donatur(models.Model):
    email = models.ForeignKey('User', models.CASCADE, db_column='email', primary_key=True)
    saldo = models.IntegerField(default=0)

    class Meta:
        db_table = 'donatur'

class Relawan(models.Model):
    email = models.ForeignKey('User', models.CASCADE, db_column='email', primary_key=True)
    no_hp = models.CharField(max_length=20)
    tanggal_lahir = models.DateField()

    class Meta:
        db_table = 'relawan'

class KeahlianRelawan(models.Model):
    email = models.ForeignKey('Relawan', models.CASCADE, db_column='email', primary_key=True)
    keahlian = models.CharField(max_length=50)

    class Meta:
        db_table = 'keahlian_relawan'
        unique_together = (('email', 'keahlian'),)