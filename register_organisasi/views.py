from django.shortcuts import render
from __future__ import unicode_literals

from django.db import connection, IntegrityError
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib import messages

from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def index(request):
    if 'username' in request.session.keys():
        html = 'layout/register-organisasi.html'
        return render(request, html)
    return HttpResponseRedirect(reverse('masuk:index'))

@csrf_exempt
def submit_form(request):
    if request.method == "POST":
        nama_orgns = request.POST['nama-orgns']
        website = request.POST['website']
        email_orgns = request.POST['email-orgns']
        kecamatan_orgns = request.POST['kecamatan-orgns']
        kabupaten_orgns = request.POST['kabupaten-orgns']
        provinsi_orgns = request.POST['provinsi-orgns']
        kode_pos_orgns = request.POST['kode-pos-orgns']
        alamat_orgns = request.POST['alamat-orgns']
        tujuan_orgns = request.POST['tujuan-orgns']
        nama_pengurus = request.POST['pengurus-orgns']
        email_pengurus = request.POST['email-pengurus']
        alamat_pengurus = request.POS['alamat-pengurus']

        try:
            cursor = connection.cursor()
            cursor.execute("SELECT * from organisasi where email_organisasi='"+email_orgns+"'")
            select = cursor.fetchone()
            if(select):
                return HttpResponse("Email sudah terdaftar")
            else:
                cursor.execute("SELECT * from organisasi where nama ='"+nama_orgns+"'")
                select = cursor.fetchone();
                if(select):
                    return HttpResponse("Nama sudah terdaftar")
                else:
                    cursor.execute("INSERT INTO organisasi VALUES(%s, %s, %s, %s,%s, %s, %s, %s, %s)",
                                   [email_orgns, website, nama_orgns, provinsi_orgns, kabupaten_orgns, kecamatan_orgns,
                                    alamat_orgns, kode_pos_orgns, "Aktif"])
                    cursor.execute("INSERT INTO pengurusorganisasi VALUES(%s, %s)", [email_pengurus, nama_orgns])
                    return HttpResponse("HEHEHEHEH")
        except IntegrityError:
            messages.error(request, "HEHEHEH")
    return HttpResponseRedirect(reverse('register-organisasi:index'))