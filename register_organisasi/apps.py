from django.apps import AppConfig


class RegisterOrganisasiConfig(AppConfig):
    name = 'register_organisasi'
