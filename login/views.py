from __future__ import unicode_literals

from django.db import connection
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

# Create your views here.
from django.views.decorators.csrf import csrf_exempt

def index(request):
    if 'username' in request.session.keys():
        return HttpResponseRedirect(reverse('profile:index'))
    html = 'layout/masuk.html'
    return render(request, html)

def login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        with connection.cursor() as cursor:
            cursor.execute("SELECT email, password FROM sion.user WHERE email = %s AND password = %s", [username, password])
            exist = cursor.fetchone()

        if exist:
            request.session['username'] = username

            with connection.cursor() as cursor:
                cursor.execute("SELECT email FROM relawan WHERE email = %s", [username])
                exist = cursor.fetchone()

                if exist:
                    request.session['role'] = 'relawan'

            with connection.cursor() as cursor:
                cursor.execute("SELECT email FROM donatur WHERE email = %s", [username])
                exist = cursor.fetchone()

                if exist:
                    request.session['role'] = 'donatur'

            with connection.cursor() as cursor:
                cursor.execute("SELECT email FROM sponsor WHERE email = %s", [username])
                exist = cursor.fetchone()

                if exist:
                    request.session['role'] = 'sponsor'
        else:
            messages.error(request, "Username atau Password salah!")

    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))