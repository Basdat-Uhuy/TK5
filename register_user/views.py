from __future__ import unicode_literals

from django.db import connection, IntegrityError
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

# Create your views here.
from django.views.decorators.csrf import csrf_exempt


def index(request):
    html = 'layout/daftar.html'
    return render(request, html)

def submit_form(request):
    if request.method == "POST":
        email = request.POST['email']
        password = request.POST['password']
        nama = request.POST['name']
        alamat_lengkap = request.POST['jalan'] + ", " + request.POST['kecamatan'] + ", " + \
                         request.POST['kabupaten'] + ", " + request.POST['provinsi']

        try:
            with connection.cursor() as cursor:
                cursor.execute("INSERT INTO sion.user VALUES (%s, %s, %s, %s)", [email, password, nama, alamat_lengkap])
            print(request.POST['selectRole'])

            if request.POST['selectRole'] == 'relawan':
                no_hp = request.POST['no_hp']
                tgl_lahir = request.POST['tanggal_lahir']
                keahlian = request.POST['keahlian'].split(";")

                with connection.cursor() as cursor:
                    cursor.execute("INSERT INTO relawan VALUES (%s, %s, %s)", [email, no_hp, tgl_lahir])
                    for k in keahlian:
                        cursor.execute("INSERT INTO keahlian_relawan VALUES (%s, %s)", [email, k])

            elif request.POST['selectRole'] == 'sponsor':
                logo = request.POST['logo']

                with connection.cursor() as cursor:
                    cursor.execute("INSERT INTO sponsor VALUES (%s, %s)", [email, logo])

            elif request.POST['selectRole'] == 'donatur':
                with connection.cursor() as cursor:
                    cursor.execute("INSERT INTO donatur VALUES (%s)", [email])

            messages.success(request, "Akun anda berhasil dibuat, silahkan login!")
            return HttpResponseRedirect(reverse('masuk:index'))
        except IntegrityError:
            messages.error(request, "Email sudah terdaftar!")

    return HttpResponseRedirect(reverse('daftar:index'))