from django.conf.urls import url

from .views import index, submit_form

urlpatters = [
    url(r'^', index, name='index'),
    url(r'^submit', submit_form, name='submit')
]